const app = require('express')();
const config = require('config');
const DB = require('tingodb')({
    nativeObjectID: true
}).Db;
const db = new DB(require('path').resolve(config.db), {
});

const api = require('./api')(db);

app.use(require('morgan')('combined'));

app.use(
    '/jquery',
    require('serve-static')(
        require('path').resolve(
            require.resolve('jquery/package.json'),
            '../dist'
    ))
);

app.use(
    '/leaflet',
    require('serve-static')(
        require('path').resolve(
            require.resolve('leaflet/package.json'),
            '../dist'
    ))
);
app.use(require('serve-static')(require('path').resolve(__dirname, 'static')));

app.get('/api/drones', (req, res, next) => {
    api.listDrones().then((drones) => res.json(drones), (err) => next(err));
});

var jsonParser = require('body-parser').json();

app.post('/api/drones/:droneId', jsonParser, (req, res, next) => {
    api.updateDronePosition(
        req.params.droneId,
        req.body[0],
        req.body[1]
    ).then(() => res.status(200).end(), (err) => next(err));
});

app.use('/api', (err, req, res, next) => {
    res.status(503);
    res.write(err.toString());
    res.end();
});

app.listen(config.port);