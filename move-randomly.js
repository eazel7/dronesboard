const http = require('http');
const config = require('config');
const randomPoint = require('@turf/random').randomPoint;

function updateDrone(droneId, longitude, latitude) {
    return new Promise((resolve, reject) => {
        var post_data = JSON.stringify([longitude, latitude]);

        var post_options = {
            host: 'localhost',
            port: config.port,
            path: '/api/drones/' + encodeURIComponent(droneId),
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(post_data)
            }
        };

        var post_req = http.request(post_options, function (res) {
            res.setEncoding('utf8');
            res.on('end', () => {
                resolve();
            });
            res.resume();
        });

        post_req.write(post_data);
        post_req.end();
    });
}

function listDrones() {
    return new Promise((resolve, reject) => {
        var get_options = {
            host: 'localhost',
            port: config.port,
            path: '/api/drones',
            method: 'GET',
            headers: {
            }
        };

        try {
            var req = http.request(get_options, function (res) {
                res.setEncoding('utf8');
                var buffer = new Buffer('');

                res.on('data', (data) => {
                    buffer = Buffer.concat([buffer, Buffer.from(data)]);
                });

                res.on('end', () => {
                    try {
                        var data = JSON.parse(buffer.toString());

                        resolve(data);
                    } catch (e) {
                        reject(e);
                    }
                })
            });

            req.end();
        } catch (e) {
            reject(e);
        }
    });
}

var maxDrones = 200;

listDrones().then((drones) => {
    var byId = {};

    drones.forEach((d) => byId[d._id] = d);

    for (var i = Object.keys(byId).length; i < maxDrones; i++) {
        byId['drone' + i.toFixed(0)] = {};
    }

    for (var id in byId) {
        var box = [-90, -180, 90, 180];

        if (byId[id].latitude && byId[id].latitude) {
            if (Math.random() < 0.2) continue; // 20% chance of sending then same position

            box = [byId[id].longitude - 0.0005, byId[id].latitude - 0.0005, byId[id].longitude + 0.0005, byId[id].latitude + 0.0005];
            // box = [byId[id].latitude - 0.000005, byId[id].longitude - 0.000005, byId[id].latitude + 0.000005, byId[id].longitude + 0.000005];
        }

        var newPoint = randomPoint(1, { bbox: box });

        byId[id].longitude = newPoint.features[0].geometry.coordinates[0];
        byId[id].latitude = newPoint.features[0].geometry.coordinates[1];
    }

    var pending = Object.keys(byId);

    function doNext() {
        if (pending.length === 0) return process.exit();

        var current = pending.splice(0, 1)[0];

        updateDrone(current, byId[current].longitude, byId[current].latitude)
            .then(() => doNext());
    }

    doNext();
});