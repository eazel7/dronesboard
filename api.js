const turf = {
    distance: require('@turf/distance').default,
    point: require('@turf/helpers').point
};

function API(db) {
    if (!db) throw new Error('db is required');

    this.drones = db.collection('drones');
}

API.prototype.listDrones = function () {
    return new Promise((resolve, reject) => {
        this.drones.find({}).toArray((err, docs) => {
            if (err) return reject(err);

            resolve(docs);
        })
    });
}

API.prototype.updateDronePosition = function (droneId, longitude, latitude) {
    return new Promise((resolve, reject) => {
        this.drones.findOne({ _id: droneId }, (err, doc) => {
            if (err) return reject(err);

            var currentTime = Date.now();

            // speed in seconds
            var speed = 0;
            
            if (doc && doc.lastReported) {
                var distanceInMeters = turf.distance(turf.point([doc.longitude, doc.latitude]), turf.point([longitude, latitude]), 'kilometers') * 1000;
                var secondsElapsed = (currentTime - doc.lastReported) / 1000;

                speed = distanceInMeters / secondsElapsed;
            }

            this.drones.update({
                _id: droneId
            }, {
                    $set: {
                        longitude: longitude,
                        latitude: latitude,
                        lastReported: currentTime,
                        speed: speed
                    }
                }, {
                   upsert: true 
                }, (err) => {
                    if (err) return reject(err);

                    resolve();
                })
        });
    });
}

module.exports = (db) => {
    return new API(db);
}