const API = require('../api');
const assert = require('assert');
const tingodb = require('tingodb')({
    memStore: true,
    nativeObjectID: true
});
const turf = {
    distance: require('@turf/distance').default,
    point: require('@turf/helpers').point
};

describe('API', () => {
    it('requires db', () => {
        assert.throws(() => API(null), /db is required/);
    });

    context('accepts db', () => {
        let db;
        let api;

        beforeEach((done) => {
            db = new tingodb.Db('drones', {});

            db.collection('drones')
                .remove({}, { multi: true }, (err) => {
                    if (err) return done(err);

                    db.collection('drones').insert({
                        _id: 'abcd1234',
                        prop: 'any'
                    }, (err) => {
                        if (err) return done(err);

                        api = API(db);
                        done();
                    });
                });
        });

        it('accepts db', () => {
            API(db);
        });

        it('lists drones', () => {
            return api.listDrones().then((drones) => {
                assert.deepEqual([{
                    _id: 'abcd1234',
                    prop: 'any'
                }], drones);
            })
        });

        it('updates drone position', () => {
            return api.updateDronePosition('abcd1234', -12.3456, 78.90123).then(() => {
                return new Promise((resolve, reject) => {
                    db.collection('drones').count({
                        _id: 'abcd1234',
                        longitude: -12.3456,
                        latitude: 78.90123,
                        speed: 0,
                        lastReported: {
                            $exists: true
                        }
                    }, (err, count) => {
                        if (err) return reject(err);
                        if (count == 1) return resolve();

                        reject(new Error('document not updated'));
                    });
                });
            });
        });

        it('creates new drone', () => {
            return api.updateDronePosition('ididnexistbefore', -12.3456, 78.90123).then(() => {
                return new Promise((resolve, reject) => {
                    db.collection('drones').count({
                        _id: 'ididnexistbefore',
                        longitude: -12.3456,
                        latitude: 78.90123,
                        speed: 0,
                        lastReported: {
                            $exists: true
                        }
                    }, (err, count) => {
                        if (err) return reject(err);
                        if (count == 1) return resolve();

                        reject(new Error('document not created'));
                    });
                });
            });
        });

        it('updates drone speed', () => {
            return api.updateDronePosition('abcd1234', -12.3456, 78.90123)
                .then(() => new Promise((resolve, reject) => {
                    db.collection('drones').findOne({
                        _id: 'abcd1234'
                    }, (err, firstDoc) => {
                        if (err) return reject(err);

                        var firstReported = firstDoc.lastReported;

                        setTimeout(() => {
                            api.updateDronePosition('abcd1234', -12.4444, 78.7777)
                                .then(() => {
                                    db.collection('drones').findOne({
                                        _id: 'abcd1234'
                                    }, (err, secondDoc) => {
                                        if (err) return reject(err);
                                        var lastReported = secondDoc.lastReported;

                                        var distance = turf.distance(turf.point([-12.3456, 78.90123]), turf.point([-12.4444, 78.7777]), 'kilometers') * 1000;
                                        var secondsElapsed = (lastReported - firstReported) / 1000;

                                        var speedInMetersPerSecond = distance / secondsElapsed;

                                        if (speedInMetersPerSecond == secondDoc.speed) return resolve();

                                        reject(new Error('speed is different'));
                                    });
                                }, (err) => reject(err));
                        }, 1000);
                    });
                }));
        });
    });
});