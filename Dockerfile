FROM node

COPY . /src
WORKDIR /src
RUN npm install

ENV NODE_ENV demo
CMD node .