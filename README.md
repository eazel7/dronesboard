# Dronesboard

> A simple dashboard for monitoring drones in a map

## Building docker image

```
docker build -t dronesboard .
```

## Running docker (requires building image)

```
docker run -d --name dronesboard -p 8000:8000 dronesboard
```

## Injecting randomly located drones (requires running the image)

```
docker exec -ti dronesboard node move-randomly.js
```

# Endpoints

`GET /api/drones` returns JSON list of drones

`POST /api/drones/{droneId}` updates or creates a drone in the map. Takes JSON body:

```
[-12.3456, 65.4321]
```

The first number in the array is the longitude, the second is the latitude.,

The server will calculate the speed in meters per second from the last reported location and time.

`/` returns page with a map of drones. Refreshes every 1 second. Drones in red are not moving, drones in green are moving faster than 1 meter per second.