$(document).ready(() => {
    var map = L.map('map');

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    map.fitWorld();

    var markers = [];

    var greenDrone = L.icon({
        iconUrl: 'drone-green.png',

        iconSize: [96, 96],
        iconAnchor: [48, 48],
        shadowAnchor: [4, 62],
        popupAnchor: [0, -48]
    });

    var redDrone = L.icon({
        iconUrl: 'drone-red.png',

        iconSize: [96, 96],
        iconAnchor: [48, 48],
        shadowAnchor: [4, 62],
        popupAnchor: [0, -48]
    });


    function queryApi() {
        return new Promise((resolve, reject) => {
            $.ajax('/api/drones', {
                success: (data) => resolve(data),
                error: (err) => reject(err)
            });
        })
            .then((drones) => {
                markers.forEach((marker) => marker.remove());
                markers.splice(0, markers.length);

                drones.forEach(drone => {

                    let marker = L.marker([drone.longitude, drone.latitude], {
                        // less than 1 meter per second as stopped
                        icon: drone.speed < 1 ? redDrone : greenDrone,
                        title: drone._id + ' - speed: ' + drone.speed.toFixed(2)
                    });


                    markers.push(marker);
                    marker.addTo(map);
                });
            });
    }

    function refresh(every) {
        return queryApi()
            .then(() => setTimeout(() => refresh(every), every), () => setTimeout(() => refresh(every), every));
    }

    map.whenReady(() => refresh(1000));
});